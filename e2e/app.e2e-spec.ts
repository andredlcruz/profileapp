import { AndreAppMPage } from './app.po';

describe('andre-app-m App', () => {
  let page: AndreAppMPage;

  beforeEach(() => {
    page = new AndreAppMPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
